var myApp = angular.module("myApp" , ['ngCookies']);


myApp.controller("myController", function($scope,$http,$cookies,$rootScope,$window){
	$scope.données=angular.fromJson(localStorage.getItem("données"));
	$scope.user=angular.fromJson($scope.données.user);
	$scope.id=$scope.user.id;
	$scope.solde=$scope.user.leave_balance
	$scope.NewConge = {};
	$scope.message="";
	$rootScope.login="true";
	  
	 $scope.Conges = $http.get('http://wft-api.herokuapp.com/employees/'+ $scope.id +'/holidays'
		,{headers:{ 'Authorization':  $cookies.get('token')}
    }
	).then( function successCallback(response){ $scope.Conges = response.data ; console.log(JSON.stringify(response.data));}
	,function errorCallback(response){console.log('response',response);});
	  
	  
	  
	  
	  
	  
	$scope.enregistrer =function(){ 

         $http.post('http://wft-api.herokuapp.com/employees/'+$scope.id+'/holidays',angular.toJson($scope.NewConge),{
         headers:{ 'Authorization':  $cookies.get('token')}
        }).then( function successCallback(response){ console.log(response); $window.location.reload();}
		,function errorCallback(response){console.log('response',response);});
        $scope.NewConge = {};
		$scope.message="Votre demande de congés a été envoyé";  
	  };
	 
	 
	$scope.clearMessage = function(){
		  $scope.message="";
	};
	  
	$scope.Supprimer = function(id){
		   $http.defaults.headers.common.Authorization = $cookies.get('token');
		  $http.post('http://wft-api.herokuapp.com/employees/'+$scope.id+'/holidays/'+id+'/delete').then( function successCallback(response){ console.log(response);} 
		  ,function errorCallback(response){console.log('response',response);});  
		  
	};
	  
});






myApp.controller("myCtrAttestation", function($scope,$http,$rootScope,$cookies,$window){
	  $scope.NewAttes = {};
	  $scope.ClickDoc={};
	  $scope.idattes={};
	  $scope.myVar= false ;
	  $scope.données=angular.fromJson(localStorage.getItem("données"));
	  $scope.user=angular.fromJson($scope.données.user);
	  $scope.id=$scope.user.id;
	  $scope.message="";
	  $scope.tt="";
	  $rootScope.login="true";
	  
	$scope.Typedoc = $http.get('http://wft-api.herokuapp.com/document_types'
		,{headers:{ 'Authorization':  $cookies.get('token')}
    }
	).then( function successCallback(response){ $scope.Typedoc = response.data ; console.log(JSON.stringify(response.data));}
	,function errorCallback(response){console.log('response',response);});
	   
	$scope.Selectdoc = function(attes){
	  $scope.ClickDoc=attes;
	  $scope.idattes=attes.id;
	};
	  
	$scope.check=function(){
		  
		  if 
		    ($scope.myVar == true )
			{$scope.myVar = false ;}
			else{$scope.myVar = true }
	  };
	  
	  
	$scope.Attes = $http.get('http://wft-api.herokuapp.com/employees/'+ $scope.id +'/document_requests'
		,{headers:{ 'Authorization':  $cookies.get('token')}
    }
	).then( function successCallback(response){ $scope.Attes = response.data ; console.log(JSON.stringify(response.data));} 
	,function errorCallback(response){console.log('response',response);});
	   
	 
	  $scope.Demander =function(){ 
             $http.defaults.headers.common.Authorization =$cookies.get('token');
             $http.post('http://wft-api.herokuapp.com/employees/'+ $scope.id +'/document_requests',{
             'document_type_id': $scope.idattes,'raison': $scope.NewAttes.raison ,'certified': $scope.NewAttes.certified}
             ).then( function successCallback(response){$window.location.reload(); console.log(response);} 
			 ,function errorCallback(response){console.log('response',response);});
	         $scope.message="Votre demande d'attestation a été envoyé";
	  };
	  
	  
	  
	  
	  $scope.Supprimer =function(id1){ 
             $http.defaults.headers.common.Authorization =$cookies.get('token');
             $http.post('http://wft-api.herokuapp.com/employees/'+ $scope.id +'/document_requests/'+id1+'/delete',angular.toJson($scope.NewAttes)).then( function successCallback(response){ $window.location.reload(); console.log(response);} 
			 ,function errorCallback(response){console.log('response',response);});
	         $scope.message="Votre demande d'attestation a été envoyé";
	  };
	 
	 
	  $scope.clearMessage = function(){
		  $scope.message="";
	  };
	  
		  
		  
});







myApp.controller("CtrAjtInf", function($scope,$http,$cookies,$rootScope){
	$scope.NewInf ={};
	$scope.données=angular.fromJson(localStorage.getItem("données"));
	$scope.user=angular.fromJson($scope.données.user);
	$scope.id=$scope.user.id;
	$rootScope.login="true";
	 
	  
	$scope.enregistrerInf = function() {
		
		$http.defaults.headers.common.Authorization =$cookies.get('token');
		 $http.put('http://wft-api.herokuapp.com/employees/'+$scope.id ,angular.toJson($scope.NewInf)).then( function successCallback(response){  $scope.Message="l'employé est bien modifié";}
		 ,function errorCallback(response){console.log('response',response);});
	  };
	 
	  
});





myApp.controller("CtrDeconn", function($scope,$http,$cookies,$window,$rootScope){  


    $scope.logout=function(){
	
	  localStorage.removeItem('données');
	  $cookies.remove('token');
 	  window.location.href='/page.html'
	
    };



});