var myApp = angular.module("myApp" , ['ngCookies']);

/*myApp.run(['$http','$cookies' function($http,$cookies) {
    $http.defaults.headers.common['Authorization'] = $cookies.get('token') ;
}]);*/
//myApp.config(['$httpProvider', function($httpProvider) {
    //$httpProvider.defaults.headers.common['Authorization'] = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJleHAiOjE1MDMwNjQ4MjQsInVzZXIiOiJ7XCJpZFwiOjEwLFwibmFtZVwiOlwic2F3c3NlblwiLFwiZmFtaWx5X25hbWVcIjpcInlvdXNzZmlcIixcImNpblwiOjAsXCJlbWFpbFwiOlwic2F3c3NlbnlvdXNmaTJAZ21haWwuY29tXCIsXCJwYXNzd29yZFwiOlwiZWZiNGUyNjdhNzQ4NmM0ZWQxMzEyMTFjY2FlNmI5MDQ2MTg5MjE4ZlwiLFwiZ2VuZGVyXCI6XCJmZW1hbGVcIixcInBob25lMVwiOjAsXCJwaG9uZTJcIjpudWxsLFwiZGF0ZV9jaW5cIjpudWxsLFwiYWRkcmVzc1wiOlwidHVuaXNcIixcImlzX2FjdGl2ZVwiOnRydWUsXCJzdGFydF9kYXRlXCI6bnVsbCxcImVuZF9kYXRlXCI6bnVsbCxcInRpdGxlXCI6bnVsbCxcImRlc2NyaXB0aW9uXCI6bnVsbCxcImxpbmtlZGluXCI6bnVsbCxcInR3aXR0ZXJcIjpudWxsLFwiaXNfYWRtaW5cIjp0cnVlLFwicGhvdG9cIjpudWxsLFwiY3JlYXRlZF9hdFwiOlwiMjAxNy0wNy0xNFQxMDozMDoxMy40MzhaXCIsXCJ1cGRhdGVkX2F0XCI6XCIyMDE3LTA3LTE0VDEwOjMwOjEzLjQzOFpcIn0ifQ.YA5a81st7t2DZZHbXC3HTfvgYENUfd8WI71zneDoz-WCu0WYz8rvG0cnErBRw7B6AjeKqe7AMQGs27887SnDNmeqFzDEFlhHwYJTxTN0Wj_nYRyNkExY3DB-2dU3Y65-KjzWiWqgH5Yd73hN_mwID4rTiNCeWXf4TK-7NpPRXzk";
//}])

myApp.controller("AdminCtrEmp", function($scope,$http,$cookies,$window){
	
	
	$scope.données=angular.fromJson(localStorage.getItem("données"));
	$scope.user=angular.fromJson($scope.données.user);
	  $scope.NewEmploye = {};
	  $scope.ClickEmploye={};
	  $scope.Message="";
	  $scope.id={};
	  $scope.employes = [];


		

	
	$scope.Info = $http.get('http://wft-api.herokuapp.com/employees?need_leave_balance=true'
		,{
                  headers:{ 'Authorization':  $cookies.get('token')}}
	                       ).then( function successCallback(response){ $scope.employes = response.data ; console.log(JSON.stringify(response.data));} 
	    ,function errorCallback(response){console.log('response',response);});

	
	
	
	
	 $scope.EnregistrerEmp =function(){
            $http.post('http://wft-api.herokuapp.com/employees',angular.toJson($scope.NewEmploye),{
       
            headers:{ 'Authorization':  $cookies.get('token')}
            }).then( function successCallback(response){ $window.location.reload(); console.log(response); $scope.Message="Nouveau employé enregistré avec succées";} ,function errorCallback(response){$scope.Message="l'ajout de l'employé a échoué"; console.log('response',response);});

	};

	  $scope.selectEmploye = function(user){
	  console.log(user);
	  $scope.ClickEmploye=user;
	  $scope.id=user.id;
	  };
	   
	  
	  $scope.employes = $http.get('http://wft-api.herokuapp.com/employees'
		,{
        headers:{ 'Authorization':  $cookies.get('token')}}
	    ).then( function successCallback(response){ $scope.employes = response.data ; console.log(JSON.stringify(response.data));} 
	    ,function errorCallback(response){console.log('response',response);});

	  
	    
	  $scope.UpdateUser = function(id) {
		  $http.defaults.headers.common.Authorization =$cookies.get('token');
		   $http.put('http://wft-api.herokuapp.com/employees/'+id ,angular.toJson($scope.ClickEmploye)).then( function successCallback(response)
		   {  $scope.Message="l'employé est bien modifié";} ,function errorCallback(response){console.log('response',response);});
	  };
	  
	  $scope.clearMessage = function(){
		  $scope.Message="";
	  };

});



myApp.controller("myCtrConge", function($scope,$cookies,$http,$window){
	$scope.données=angular.fromJson(localStorage.getItem("données"));
	$scope.user=angular.fromJson($scope.données.user);
     $scope.tt={};
	 $scope.ClickConge={};
	 $scope.Accept="validated";
	 $scope.Refu="refused";
	 $scope.message="";  
	  
	  
    $scope.Conges1 = $http.get('http://wft-api.herokuapp.com/holidays'
    ,{headers:{ 'Authorization':  $cookies.get('token')}}
	).then( function successCallback(response){ $scope.Conges1 = response.data ; console.log(JSON.stringify(response.data));}
	,function errorCallback(response){console.log('response',response);});


    
	$scope.Accepter = function(id,id1){
	$http.defaults.headers.common.Authorization =$cookies.get('token');
    $http.post('http://wft-api.herokuapp.com/employees/'+id1+'/holidays/'+ id+'/validate' ).then( function successCallback(response){$window.location.reload(); console.log(response);   $scope.message=" la demande est accepté ";}
	,function errorCallback(response){console.log('response',response);});
	  };
	  
	  
	$http.defaults.headers.common.Authorization =$cookies.get('token');
	$scope.Refuser = function(id,id1){
       $http.post('http://wft-api.herokuapp.com/employees/'+id1+'/holidays/'+ id+'/reject' ).then( function successCallback(response){ $window.location.reload(); console.log(response);  $scope.message=" la demande est refusé";} 
	   ,function errorCallback(response){console.log('response',response);});


	  };

});








myApp.controller("myCtrAtt", function($scope,$http,$cookies,$window){
	
	 
	  $scope.newdemande = {};
	  $scope.message="";
	  
	 $scope.demandes = $http.get('http://wft-api.herokuapp.com/document_requests'
		,{headers:{ 'Authorization':  $cookies.get('token')}
    }
	).then( function successCallback(response){ $scope.demandes = response.data ; console.log(JSON.stringify(response.data));} 
	,function errorCallback(response){console.log('response',response);});

	
	  $scope.clearMessage = function(){
		  $scope.message="";
	  };
	  
	  
	  $http.defaults.headers.common.Authorization =$cookies.get('token');
	  $scope.Accepter = function(id1,id2){
		 $http.post('http://wft-api.herokuapp.com/employees/'+id2 +'/document_requests/'+id1+'/validate' ).then( function successCallback(response){$window.location.reload(); console.log(response);   $scope.message=" la demande est accepté ";}
		 ,function errorCallback(response){console.log('response',response);});  
	  };
	  
	$scope.Refuser = function(id1,id2){
		  $http.defaults.headers.common.Authorization =$cookies.get('token'); 
          $http.post('http://wft-api.herokuapp.com/employees/'+id2+'/document_requests/'+id1+'/reject').then( function successCallback(response){$window.location.reload();  console.log(response);  $scope.message=" la demande est refusé";} 
		  ,function errorCallback(response){console.log('response',response);});

	  };
	  
	  
	  
	  $http.defaults.headers.common.Authorization =$cookies.get('token');
	   $scope.Being_done = function(id1,id2){	  
       $http.post('http://wft-api.herokuapp.com/employees/'+id2+'/document_requests/'+id1+'/being_done' ).then( function successCallback(response){  $window.location.reload();console.log(response);   }
	   ,function errorCallback(response){console.log('response',response);});

		 
		  
	  };
	  $scope.Done = function(id1,id2){
		  
       $http.post('http://wft-api.herokuapp.com/employees/'+id2+'/document_requests/'+id1+'/done'
      ).then( function successCallback(response){  console.log(response);   } ,function errorCallback(response){console.log('response',response);});
 
	  };
	  
	  
	  $scope.Delivred = function(id1,id2){
		  
       $http.post('http://wft-api.herokuapp.com/employees/'+id2+'/document_requests/'+id1+'/delivre' ).then( function successCallback(response){$window.location.reload(); console.log(response);   } 
	   ,function errorCallback(response){console.log('response',response);}); 
	  };
	   
    
	
	

});





myApp.controller("CtrDeconn", function($scope,$http,$cookies,$window,$rootScope){  


  $scope.logout=function(){
	
	  localStorage.removeItem('données');
	  $cookies.remove('token');
 	  window.location.href='/page.html'
	
};



});



myApp.controller('Ctrjob', function($scope,$location,$http,$rootScope,$window){

$scope.NewJob={};
	  
$scope.JOB = $http.get('http://wft-api.herokuapp.com/job_offer'
	).then( function successCallback(response){ $scope.JOB = response.data ; console.log(JSON.stringify(response.data));} 
	,function errorCallback(response){console.log('response',response);});
	   
	 
$scope.SuppOFFRE = function(id1){
       $http.post('http://wft-api.herokuapp.com/employees/' ).then( function successCallback(response){$window.location.reload(); console.log(response);   }
	   ,function errorCallback(response){console.log('response',response);});

	  };
	  
$scope.AjoutOFFRE=function(){
		  
     $http.post('http://wft-api.herokuapp.com/job_offer',angular.toJson($scope.NewJob) ).then( function successCallback(response){ $window.location.reload(); console.log(response);   } 
	 ,function errorCallback(response){console.log('response',response);});

	  };	

		

});






